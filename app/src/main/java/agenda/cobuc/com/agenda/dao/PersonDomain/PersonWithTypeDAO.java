package agenda.cobuc.com.agenda.dao.persondomain;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import agenda.cobuc.com.agenda.lib.dao.QueryDAO;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;

public class PersonWithTypeDAO extends QueryDAO<PersonWithType> {

    public static String PERSON_WITH_TYPE_TABLE_NAME = "Person";

    public static final String NAME_PERSON_ATTR = "name";
    public static final String SURNAME1_PERSON_ATTR = "surname1";
    public static final String SURNAME2_PERSON_ATTR = "surname2";
    public static final String EMAIL_PERSON_ATTR = "email";
    public static final String PHONE_PERSON_ATTR = "phone";
    public static final String TYPE_PERSON_ATTR = "type";

    public PersonWithTypeDAO(Context context) {
        super(context,PERSON_WITH_TYPE_TABLE_NAME);
        List<String> fields = new ArrayList<String>();
        fields.add(NAME_PERSON_ATTR);
        fields.add(SURNAME1_PERSON_ATTR);
        fields.add(SURNAME2_PERSON_ATTR);
        fields.add(EMAIL_PERSON_ATTR);
        fields.add(PHONE_PERSON_ATTR);
        fields.add(TYPE_PERSON_ATTR);


        INSERT_QUERY = insertQueryBuilder(fields, PERSON_WITH_TYPE_TABLE_NAME);
    }

    public List<PersonWithType> getAll() {
        List<PersonWithType> personList = super.getAll();
        return personList;
    }

    @Override
    protected PersonWithType loadItemFromCursor(Cursor cursor) {

        String name = cursor.getString(cursor.getColumnIndex(NAME_PERSON_ATTR));
        String surname1 = cursor.getString(cursor.getColumnIndex(SURNAME1_PERSON_ATTR));
        String surname2 = cursor.getString(cursor.getColumnIndex(SURNAME2_PERSON_ATTR));
        String email = cursor.getString(cursor.getColumnIndex(EMAIL_PERSON_ATTR));
        String phone = cursor.getString(cursor.getColumnIndex(PHONE_PERSON_ATTR));
        String type = cursor.getString(cursor.getColumnIndex(TYPE_PERSON_ATTR));

        return new PersonWithType(name, surname1, surname2, email, phone, new PersonType(type));
    }

    @Override
    protected SQLiteStatement loadBindings(SQLiteStatement statement, PersonWithType item) {
        statement.bindString(1, item.getName());
        statement.bindString(2, item.getSurname1());
        statement.bindString(3, item.getSurname2());
        statement.bindString(4, item.getEmail());
        statement.bindString(5, item.getPhone());
        statement.bindString(6, item.getType().getName());
        return statement;
    }
}
