package agenda.cobuc.com.agenda.lib.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseDAO extends SQLiteOpenHelper {

    private Context context;
    public static final String DB_NAME = "DBLite.db";
    protected static final int DB_VERSION = 1;
    protected SQLiteDatabase myDataBase;

    public BaseDAO(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

    public void copyDataBase() throws IOException {
        synchronized (this) {
            // Open DB as the ReadOnly
            myDataBase = getReadableDatabase();

            // Open your local DB as the input stream
            InputStream myInput = context.getAssets().open(DB_NAME);

            // Path to the just created empty DB
            String outFileName = context.getDatabasePath(DB_NAME).getPath();

            // Open the empty DB as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            // transfer bytes from the inputFile to the outputFile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
    }

    protected void openDBRead() {
        this.myDataBase = getReadableDatabase();
    }

    protected void openDBWrite() {
        this.myDataBase = getWritableDatabase();
    }

    protected void closeDB() {
        if (this.myDataBase != null)
            this.myDataBase.close();
        super.close();
    }

    public boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(
                    context.getDatabasePath(DB_NAME).getPath(), null,
                    SQLiteDatabase.OPEN_READONLY);
            checkDB.close();
        } catch (SQLiteException e) {
            // database doesn't exist yet.
        }
        return checkDB != null ? true : false;
    }

    public boolean isTableExists(String tableName, boolean openDb) {
        if (openDb) {
            if (myDataBase == null || !myDataBase.isOpen()) {
                myDataBase = getReadableDatabase();
            }

            if (!myDataBase.isReadOnly()) {
                myDataBase.close();
                myDataBase = getReadableDatabase();
            }
        }

        Cursor cursor = myDataBase.rawQuery(
                "select DISTINCT tbl_name from sqlite_master where tbl_name = '"
                        + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public boolean isAnyTable(boolean openDb) {
        if (openDb) {
            if (myDataBase == null || !myDataBase.isOpen()) {
                myDataBase = getReadableDatabase();
            }

            if (!myDataBase.isReadOnly()) {
                myDataBase.close();
                myDataBase = getReadableDatabase();
            }
        }

        Cursor cursor = myDataBase.rawQuery(
                "select DISTINCT tbl_name from sqlite_master", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

}