package agenda.cobuc.com.agenda.models;

/**
 * Created by vicaba on 06/08/15.
 */
public class FavWebsite {

    private String label;

    private String url;

    public FavWebsite(String label, String url) {
        this.label = label;
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }
}
