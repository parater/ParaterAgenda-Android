package agenda.cobuc.com.agenda.models;


public class PersonType {

    private String name;

    public PersonType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof PersonType) {
            PersonType _pt = (PersonType) o;
            return this.getName().equals(_pt.getName());
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return this.getName().hashCode();
    }
}
