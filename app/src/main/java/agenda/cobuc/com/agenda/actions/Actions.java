package agenda.cobuc.com.agenda.actions;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.net.URI;

/**
 * Singleton object for actions
 */
public class Actions {

    public static Actions getInstance() {
        return new Actions();
    }

    public void openInBrowser(Context context, Uri url) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, url);
        context.startActivity(browserIntent);
    }

    /**
     * Makes a call to a phone number
     * @param context   the app context
     * @param number    the phone number
     */
    public void call(Context context, String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            context.startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Call action failed", "Actions failed", activityException);
        }
    }

    /**
     * Sends an email to the specified email
     * @param context   the app context
     * @param recipient the recipient of the email
     */
    public void sendEmail(Context context, String recipient) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{recipient});
        i.putExtra(Intent.EXTRA_SUBJECT, "A subject");
        i.putExtra(Intent.EXTRA_TEXT   , "A body");
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
