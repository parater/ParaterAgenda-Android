package agenda.cobuc.com.agenda.activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ArrayAdapter;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import agenda.cobuc.com.agenda.activity.alerts.AlertFactory;
import agenda.cobuc.com.agenda.adapter.FavWebsiteListAdapter;
import agenda.cobuc.com.agenda.dao.FavWebsiteDAO;
import agenda.cobuc.com.agenda.lib.dao.QueryDAO;
import agenda.cobuc.com.agenda.models.FavWebsite;
import agenda.cobuc.com.agenda.models.FavWebsiteSingleton;

public class FavWebsiteActivityHelper {

    public static void loadDataFromDatabase(
            final Context context,
            final List<FavWebsite> data,
            final FavWebsiteListAdapter adapter,
            final FavWebsiteDAO queryDAO) {
        synchronized (data) {
            data.clear();
            data.addAll(queryDAO.getAll());
        }
        adapter.notifyDataSetChanged();
    }

    public static void loadDataFromWebService(
            final Context context,
            final List<FavWebsite> data,
            final FavWebsiteListAdapter adapter,
            final FavWebsiteDAO queryDAO,
            final SwipeRefreshLayout swipeRefresher) {

        // try to make a WebService call
        new agenda.cobuc.com.agenda.WebServices.FavWebsite().getFavWebsiteList(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray json) {
                // if the ws returns something, delete all data from the database and update it

                try {
                    synchronized (data) {
                        List<FavWebsite> jsonResponse = FavWebsiteSingleton.getInstance().fromJson(json);
                        data.clear();

                        queryDAO.deleteAll();

                        queryDAO.insert(jsonResponse);

                        data.addAll(jsonResponse);
                        swipeRefresher.setRefreshing(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    synchronized (data) {
                        loadDataFromWebServiceErrorRoutine(context, data, adapter, queryDAO, swipeRefresher);
                    }
                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                synchronized (data) {
                    loadDataFromWebServiceErrorRoutine(context, data, adapter, queryDAO, swipeRefresher);
                }
            }
        }, context);
    }

    public static void loadDataFromWebServiceErrorRoutine(
            final Context context,
            final List<FavWebsite> data,
            final FavWebsiteListAdapter adapter,
            final FavWebsiteDAO queryDAO,
            final SwipeRefreshLayout swipeRefresher) {
        FavWebsiteActivityHelper.loadDataFromDatabase(context, data, adapter, queryDAO);
        adapter.notifyDataSetChanged();
        swipeRefresher.setRefreshing(false);
        AlertFactory.cantConnectToWs(context).show();
    }

}
