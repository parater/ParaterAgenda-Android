package agenda.cobuc.com.agenda.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.dao.FavWebsiteDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonTypeDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonWithTypeDAO;
import agenda.cobuc.com.agenda.lib.dao.BaseDAO;
import agenda.cobuc.com.agenda.models.FavWebsite;
import agenda.cobuc.com.agenda.models.FavWebsiteSingleton;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;
import agenda.cobuc.com.agenda.models.PersonWithTypeSingleton;
import agenda.cobuc.com.agenda.util.Tuple2;

public class SplashScreenActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        this.showSplashScreenDelayed();
        this.startRoutine();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    private void showSplashScreenDelayed() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreenActivity.this, MainMenuActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.staticfadeout, R.anim.fadein);

                finish();
            }
        }, 2000);
    }

    public void startRoutine() {
        this.databaseStartRoutine();
        loadDataFromWebService();
    }

    private void databaseStartRoutine() {
        BaseDAO baseDAO = new BaseDAO(getApplicationContext());
        if (!baseDAO.checkDataBase()) {
            try {
                baseDAO.copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void loadDataFromWebService() {
        loadPeopleDataFromWebService();
        loadFavWebsiteDataFromWebService();
    }

    private void loadPeopleDataFromWebService() {

        final PersonWithTypeDAO personWithTypeDAO = new PersonWithTypeDAO(this.getApplicationContext());
        final PersonTypeDAO personTypeDAO = new PersonTypeDAO(this.getApplicationContext());
        // try to make a WebService call
        new agenda.cobuc.com.agenda.WebServices.Person().getPersonList(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray json) {
                // if the ws returns something, delete all data from the database and update it
                try {
                        List<PersonWithType> jsonResponse = PersonWithTypeSingleton.getInstance().fromJson(json);
                        Tuple2<List<PersonWithType>, List<PersonType>> tuple = PersonWithTypeSingleton.split(jsonResponse);

                        personTypeDAO.deleteAll();
                        personWithTypeDAO.deleteAll();

                        personTypeDAO.insert(tuple._2);
                        personWithTypeDAO.insert(tuple._1);

                } catch (JSONException e) {
                    e.printStackTrace();
                        loadDataFromWebServiceErrorRoutine();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                    loadDataFromWebServiceErrorRoutine();
            }
        }, getApplicationContext());
    }

    private void loadFavWebsiteDataFromWebService() {

        final FavWebsiteDAO favWebsiteDAO = new FavWebsiteDAO(this.getApplicationContext());
        // try to make a WebService call
        new agenda.cobuc.com.agenda.WebServices.FavWebsite().getFavWebsiteList(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray json) {
                // if the ws returns something, delete all data from the database and update it
                try {
                    List<FavWebsite> jsonResponse = FavWebsiteSingleton.getInstance().fromJson(json);

                    favWebsiteDAO.deleteAll();
                    favWebsiteDAO.insert(jsonResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                    loadDataFromWebServiceErrorRoutine();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDataFromWebServiceErrorRoutine();
            }
        }, getApplicationContext());
    }

    private void loadDataFromWebServiceErrorRoutine() {}
}
