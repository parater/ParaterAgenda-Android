package agenda.cobuc.com.agenda.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.adapter.MainMenuListAdapter;
import agenda.cobuc.com.agenda.adapter.MenuItem;

public class MainMenuActivity extends BaseActivity {

    private ListView listView;

    private List<MenuItem> data;

    private MainMenuListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    protected void setActionBarEssentials() {
        ActionBar ab = getSupportActionBar();
        ab.setIcon(R.drawable.ic_launcher);
    }

    @Override
    protected void prepareView() {
        this.setContentView(R.layout.activity_menu_list);
    }

    @Override
    protected void associateControls() {
        this.listView = (ListView) this.findViewById(R.id.activity_menu_listview);
    }

    @Override
    protected void prepareActivity() {
        this.data = new ArrayList<>();
        this.fillData();
        this.adapter = new MainMenuListAdapter(MainMenuActivity.this, R.layout.menu_list_item, this.data);
        this.listView.setAdapter(adapter);
    }

    private void fillData() {
        this.data.add(
                new MenuItem("Directorio", R.drawable.ic_action_person, new View.OnClickListener() {
                    public void onClick(View view) {

                        Intent i = new Intent(view.getContext(),
                                PersonTypeListActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        view.getContext().startActivity(i);
                    }
                })
        );

        this.data.add(
                new MenuItem("Webs de interés", R.drawable.ic_action_link, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(view.getContext(),
                                FavWebsiteActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        view.getContext().startActivity(i);
                    }
                })
        );
    }
}
