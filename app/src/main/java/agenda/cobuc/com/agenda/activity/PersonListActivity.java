package agenda.cobuc.com.agenda.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;

import agenda.cobuc.com.agenda.activity.alerts.AlertFactory;
import agenda.cobuc.com.agenda.dao.persondomain.PersonTypeDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonWithTypeDAO;
import agenda.cobuc.com.agenda.models.Person;
import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.adapter.PersonListAdapter;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;
import agenda.cobuc.com.agenda.models.PersonWithTypeSingleton;
import agenda.cobuc.com.agenda.util.Tuple2;

public class PersonListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ListView listView;

    private List<Person> data;

    private PersonListAdapter adapter;

    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void prepareView() {
        this.setContentView(R.layout.activity_agenda_list);

    }

    @Override
    protected void associateControls() {
        this.swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        this.listView = (ListView) findViewById(R.id.person_type_listview);
    }

    @Override
    protected void prepareActivity() {

        data = new LinkedList<Person>();
        adapter = new PersonListAdapter(PersonListActivity.this, R.layout.agenda_list_item, data);
        this.listView.setAdapter(adapter);
        this.configureSwipeRefresher();
        this.loadDataFromDatabase();
        //this.loadDataFromWebService();
    }

    private void loadDataFromDatabase() {
        final PersonWithTypeDAO personWithTypeDAO = new PersonWithTypeDAO(this.getApplicationContext());
        synchronized (data) {
            data.clear();
            data.addAll(
                    personWithTypeDAO.getByParam(PersonWithTypeDAO.TYPE_PERSON_ATTR,
                            this.getIntent().getExtras().getString(PersonTypeListActivity.EXTRA_PERSON_TYPE))
            );
        }
        adapter.notifyDataSetChanged();
    }

    private void loadDataFromWebService() {

        final PersonWithTypeDAO personWithTypeDAO = new PersonWithTypeDAO(this.getApplicationContext());
        final PersonTypeDAO personTypeDAO = new PersonTypeDAO(this.getApplicationContext());

        // try to make a WebService call
        new agenda.cobuc.com.agenda.WebServices.Person().getPersonList(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray json) {
                // if the ws returns something, delete all data from the database and update it

                try {
                    synchronized (data) {
                        List<PersonWithType> jsonResponse = PersonWithTypeSingleton.getInstance().fromJson(json);
                        Tuple2<List<PersonWithType>, List<PersonType>> tuple = PersonWithTypeSingleton.split(jsonResponse);

                        data.clear();

                        personTypeDAO.deleteAll();
                        personWithTypeDAO.deleteAll();


                        personTypeDAO.insert(tuple._2);
                        personWithTypeDAO.insert(tuple._1);

                        data.addAll(personWithTypeDAO.getByParam(PersonWithTypeDAO.TYPE_PERSON_ATTR,
                                PersonListActivity.this.getIntent().getExtras().getString(PersonTypeListActivity.EXTRA_PERSON_TYPE)));

                        swipeRefresh.setRefreshing(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    synchronized (data) {
                        loadDataFromWebServiceErrorRoutine();
                    }
                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                synchronized (data) {
                    loadDataFromWebServiceErrorRoutine();
                }
            }
        }, getApplicationContext());
    }

    private void loadDataFromWebServiceErrorRoutine() {
        final PersonWithTypeDAO personWithTypeDAO = new PersonWithTypeDAO(this.getApplicationContext());
        data.clear();
        data.addAll(personWithTypeDAO.getByParam(PersonWithTypeDAO.TYPE_PERSON_ATTR,
                this.getIntent().getExtras().getString(PersonTypeListActivity.EXTRA_PERSON_TYPE)));
        adapter.notifyDataSetChanged();
        swipeRefresh.setRefreshing(false);
        AlertFactory.cantConnectToWs(PersonListActivity.this).show();
    }

    private void configureSwipeRefresher() {
        this.swipeRefresh.setOnRefreshListener(this);
        this.swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_red_light);
    }

    @Override
    public void onRefresh() {
        loadDataFromWebService();
    }

}