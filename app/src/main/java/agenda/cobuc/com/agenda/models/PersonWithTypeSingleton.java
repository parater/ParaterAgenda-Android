package agenda.cobuc.com.agenda.models;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import agenda.cobuc.com.agenda.util.JSONSerializableInterface;
import agenda.cobuc.com.agenda.util.Tuple2;

public class PersonWithTypeSingleton implements JSONSerializableInterface<PersonWithType> {

    private static PersonWithTypeSingleton sInstance = null;

    public static PersonWithTypeSingleton getInstance() {
        if (sInstance == null) {
            sInstance = new PersonWithTypeSingleton();
        }
        return sInstance;
    }

    @Override
    public PersonWithType fromJson(JSONObject json) throws JSONException {
        String name = json.getString("name");
        String surname1 = json.getString("surname1");
        String surname2 = json.getString("surname2");
        String phone = json.getString("phone");
        String email = json.getString("email");
        String typeName = json.getString("type");
        return new PersonWithType(name, surname1, surname2, phone, email, new PersonType(typeName));
    }

    @Override
    public List<PersonWithType> fromJson(JSONArray json) throws JSONException {

        List<PersonWithType> data = new LinkedList<>();

        int length = json.length();
        for (int i = 0; i < length; i++) {
            data.add(PersonWithTypeSingleton.getInstance().fromJson(json.getJSONObject(i)));
        }
        return data;
    }

    @Override
    public JSONObject toJson(PersonWithType object) {
        return null;
    }

    public static Tuple2<PersonWithType, PersonType> split(PersonWithType pwt) {
        PersonType pt = new PersonType(pwt.getType().getName());
        return new Tuple2<PersonWithType, PersonType>(
                new PersonWithType(pwt.getName(), pwt.getSurname1(), pwt.getSurname2(), pwt.getPhone(), pwt.getEmail(), pt),
                pt
        );
    }

    public static Tuple2<List<PersonWithType>, List<PersonType>> split(Collection<? extends PersonWithType> collection) {
        List<PersonWithType> pl = new ArrayList<>();
        List<PersonType> ptl = new ArrayList<>();
        Tuple2<PersonWithType, PersonType> splitted;
        for (PersonWithType pwt : collection) {
            splitted = split(pwt);

            pl.add(splitted._1);
            ptl.add(splitted._2);
        }

        return new Tuple2<>(pl, ptl);
    }
}
