package agenda.cobuc.com.agenda.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import agenda.cobuc.com.agenda.util.Tuple2;


public class PersonWithType extends Person {

    private PersonType type;

    public PersonWithType(String name, String surname1, String surname2, String phone, String email, PersonType type) {
        super(name, surname1, surname2, phone, email);
        this.type = type;
    }

    public static PersonWithType combine(Person p, PersonType pt) {
        return
                new PersonWithType(p.getName(), p.getSurname1(), p.getSurname2(), p.getPhone(), p.getEmail(), new PersonType(pt.getName()));
    }

    public PersonType getType() {
        return type;
    }



}
