package agenda.cobuc.com.agenda.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;

import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.activity.alerts.AlertFactory;
import agenda.cobuc.com.agenda.adapter.FavWebsiteListAdapter;
import agenda.cobuc.com.agenda.dao.FavWebsiteDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonTypeDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonWithTypeDAO;
import agenda.cobuc.com.agenda.models.FavWebsite;
import agenda.cobuc.com.agenda.models.FavWebsiteSingleton;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;
import agenda.cobuc.com.agenda.models.PersonWithTypeSingleton;
import agenda.cobuc.com.agenda.util.Tuple2;


public class FavWebsiteActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ListView listView;

    private List<FavWebsite> data;

    private FavWebsiteListAdapter adapter;

    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected void prepareView() {
        this.setContentView(R.layout.activity_favwebsite_list);

    }

    @Override
    protected void associateControls() {
        this.swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.favwebsite_list_swipeRefreshLayout);
        this.listView = (ListView) findViewById(R.id.favwebsite_listview);
    }

    @Override
    protected void prepareActivity() {

        data = new LinkedList<FavWebsite>();
        adapter = new FavWebsiteListAdapter(FavWebsiteActivity.this, R.layout.favwebsite_list_item, data);
        this.listView.setAdapter(adapter);
        this.configureSwipeRefresher();
        this.loadDataFromDatabase();
        //this.loadDataFromWebService();
    }

    private void loadDataFromDatabase() {
        final FavWebsiteDAO favWebsiteDAO = new FavWebsiteDAO(this.getApplicationContext());
        FavWebsiteActivityHelper.loadDataFromDatabase(FavWebsiteActivity.this, data, adapter, favWebsiteDAO);
    }

    private void loadDataFromWebService() {
        final FavWebsiteDAO favWebsiteDAO = new FavWebsiteDAO(this.getApplicationContext());

        FavWebsiteActivityHelper.loadDataFromWebService(
                FavWebsiteActivity.this, data, adapter, favWebsiteDAO, swipeRefresh
        );
    }

    private void loadDataFromWebServiceErrorRoutine() {
        FavWebsiteActivityHelper.loadDataFromWebServiceErrorRoutine(
                FavWebsiteActivity.this, data, adapter, new FavWebsiteDAO(this.getApplicationContext()), swipeRefresh
        );
    }

    private void configureSwipeRefresher() {
        this.swipeRefresh.setOnRefreshListener(this);
        this.swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_red_light);
    }

    @Override
    public void onRefresh() {
        loadDataFromWebService();
    }

}
