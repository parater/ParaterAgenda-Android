package agenda.cobuc.com.agenda.dao;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import agenda.cobuc.com.agenda.lib.dao.QueryDAO;
import agenda.cobuc.com.agenda.models.FavWebsite;

public class FavWebsiteDAO extends QueryDAO<FavWebsite> {

    public static final String FAVWEBSITE_TABLE_NAME = "FavWebsite";

    public static final String LABEL_ATTR = "label";
    public static final String URL_ATTR = "url";


    public FavWebsiteDAO(Context context) {
        super(context, FAVWEBSITE_TABLE_NAME);
        List<String> fields = new ArrayList<String>();
        fields.add(LABEL_ATTR);
        fields.add(URL_ATTR);


        INSERT_QUERY = insertQueryBuilder(fields, FAVWEBSITE_TABLE_NAME);
    }

    @Override
    protected FavWebsite loadItemFromCursor(Cursor cursor) {
        String label = cursor.getString(cursor.getColumnIndex(LABEL_ATTR));
        String url = cursor.getString(cursor.getColumnIndex(URL_ATTR));


        return new FavWebsite(label, url);
    }

    @Override
    protected SQLiteStatement loadBindings(SQLiteStatement statement, FavWebsite item) {
        statement.bindString(1, item.getLabel());
        statement.bindString(2, item.getUrl());
        return statement;
    }
}
