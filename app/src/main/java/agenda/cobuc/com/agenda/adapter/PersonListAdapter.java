package agenda.cobuc.com.agenda.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import agenda.cobuc.com.agenda.models.Person;
import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.actions.Actions;

public class PersonListAdapter extends ArrayAdapter<Person> {

    private List<Person> data;

    private int layout;

    private Context context;

    public PersonListAdapter(Context context, int resource, List<Person> data) {
        super(context, resource);
        this.context = context;
        this.data = data;
        this.layout = resource;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Person getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        // 1 .Create view
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(this.layout, parent, false);
            row.setTag(this.data.get(position).getName());

        }

        // 2. Capture controls of the view
        TextView name = (TextView) row.findViewById(R.id.person_name);
        ImageButton phone = (ImageButton) row.findViewById(R.id.person_phone);
        ImageButton email = (ImageButton) row.findViewById(R.id.person_email);

        name.setText(getItem(position).getName());

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.getInstance().call(context, getItem(position).getPhone());
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actions.getInstance().sendEmail(context, getItem(position).getEmail());
            }
        });
        return row;
    }

}
