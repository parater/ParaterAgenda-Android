package agenda.cobuc.com.agenda.WebServices;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

/**
 * Created by vicaba on 06/08/15.
 */
public class FavWebsite {

    private String domain = "http://192.168.1.36:9000";
    private String personListUri = "/favWebsite/";

    public void getFavWebsiteList(
            Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener,
            Context context) {

        String favWebSiteUrl = domain + personListUri;

        JsonArrayRequest favWebsiteRequest =
                new JsonArrayRequest(Request.Method.GET, favWebSiteUrl,
                        responseListener, errorListener);

        VolleySingleton.getInstance(context).addToRequestQueue(favWebsiteRequest);

    }
}
