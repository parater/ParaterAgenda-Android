package agenda.cobuc.com.agenda.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import agenda.cobuc.com.agenda.util.JSONSerializableInterface;

public class FavWebsiteSingleton implements JSONSerializableInterface<FavWebsite> {

    private static FavWebsiteSingleton sInstance = null;

    public static FavWebsiteSingleton getInstance() {
        if (sInstance == null) {
            sInstance = new FavWebsiteSingleton();
        }
        return sInstance;
    }

    @Override
    public FavWebsite fromJson(JSONObject json) throws JSONException {
        String label = json.getString("label");
        String url = json.getString("url");

        return new FavWebsite(label, url);
    }

    @Override
    public List<FavWebsite> fromJson(JSONArray json) throws JSONException {

        List<FavWebsite> data = new LinkedList<>();

        int length = json.length();
        for (int i = 0; i < length; i++) {
            data.add(FavWebsiteSingleton.getInstance().fromJson(json.getJSONObject(i)));
        }
        return data;
    }

    @Override
    public JSONObject toJson(FavWebsite object) {
        return null;
    }
}
