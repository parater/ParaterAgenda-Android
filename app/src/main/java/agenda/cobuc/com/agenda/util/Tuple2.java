package agenda.cobuc.com.agenda.util;

public class Tuple2<T, M> {

    public T _1;
    public M _2;

    public Tuple2(T _1, M _2) {
        this._1 = _1;
        this._2 = _2;
    }

}
