package agenda.cobuc.com.agenda.lib.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import agenda.cobuc.com.agenda.util.Tuple2;

public abstract class QueryDAO<T> extends BaseDAO {

    protected String table;
    protected String INSERT_QUERY;

    protected QueryDAO(Context context, String table) {
        super(context);
        this.table = table;
    }

    protected String insertQueryBuilder(List<String> fields, String table) {

        int valueCount = fields.size();
        String query = "INSERT INTO " + table + " (" + fields.get(0);

        for(int i = 1; i < valueCount; i++) {
            query = query + "," + fields.get(i);
        }
        query += ") VALUES (?";
        for(int i = 1; i < valueCount; i++) {
            query += ",?";
        }
        query += ");";
        return query;
    }


    /* SELECTS */
    public List<T> getAll() {
        List<T> itemList = new ArrayList<T>();

        this.openDBRead();

        String sql = "SELECT * FROM " + table;
        Cursor cursor = this.myDataBase.rawQuery(sql, null);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            T a = loadItemFromCursor(cursor);
            itemList.add(a);
            cursor.moveToNext();
        }
        cursor.close();
        this.closeDB();
        return itemList;
    }

    public List<T> getByParam(String param, String value) {
        List<T> itemList = new ArrayList<T>();
        this.openDBRead();

        String sql = "SELECT * FROM " + table + " WHERE " + param + " = ?";
        String[] fields = { value };
        Cursor cursor = this.myDataBase.rawQuery(sql, fields);
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            T a = loadItemFromCursor(cursor);
            itemList.add(a);
            cursor.moveToNext();
        }
        cursor.close();
        this.closeDB();
        return itemList;
    }

    abstract protected T loadItemFromCursor(Cursor cursor);

    /* INSERTS */
    public void insert(List<T> list) {

        this.openDBWrite();

        SQLiteStatement statement = null;
        try {
            statement = myDataBase.compileStatement(INSERT_QUERY);
        } catch (Exception e) {
            e.printStackTrace();
        }

        myDataBase.beginTransaction();

        try {
            for (T item : list) {
                statement.clearBindings();
                loadBindings(statement, item);
                statement.execute();
            }
            myDataBase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            myDataBase.endTransaction();
            this.closeDB();
        }
    }

    public void deleteAll() {
        this.openDBWrite();
        myDataBase.delete(table, "1", null);
        this.closeDB();
    }

    public void deleteByParam(String param, String value) {
        this.openDBWrite();
        myDataBase.delete(table, param + " = ?", new String[] {value});
        this.closeDB();
    }

    /*
     * @param contentValues: first position := key, second position := value
     */
    public void updateByParam(String param, String value, List<Tuple2<String, String>> contentValues) {
        this.openDBWrite();
        ContentValues dataToInsert = new ContentValues();

        for(Tuple2<String, String> paramAndValue: contentValues) {
            dataToInsert.put(paramAndValue._1, paramAndValue._2);
        }

        myDataBase.update(table, dataToInsert, param + " = ?", new String[] {value});
        this.closeDB();
    }

    protected abstract SQLiteStatement loadBindings(SQLiteStatement statement, T item);
}

