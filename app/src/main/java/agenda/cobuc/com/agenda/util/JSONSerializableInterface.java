package agenda.cobuc.com.agenda.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public interface JSONSerializableInterface<T> {

    public T fromJson(JSONObject json) throws JSONException;

    public List<T> fromJson(JSONArray json) throws JSONException;

    public JSONObject toJson(T object);

}
