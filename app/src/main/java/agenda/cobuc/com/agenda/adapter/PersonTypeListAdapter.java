package agenda.cobuc.com.agenda.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import agenda.cobuc.com.agenda.activity.PersonListActivity;
import agenda.cobuc.com.agenda.activity.PersonTypeListActivity;
import agenda.cobuc.com.agenda.models.Person;
import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.actions.Actions;
import agenda.cobuc.com.agenda.models.PersonType;

public class PersonTypeListAdapter extends ArrayAdapter<PersonType> {

    private List<PersonType> data;

    private int layout;

    private Context context;

    public PersonTypeListAdapter(Context context, int resource, List<PersonType> data) {
        super(context, resource);
        this.context = context;
        this.data = data;
        this.layout = resource;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public PersonType getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        // 1 .Create view
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(this.layout, parent, false);
            row.setTag(this.data.get(position));

        }

        row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(),
                        PersonListActivity.class);
                i.putExtra(PersonTypeListActivity.EXTRA_PERSON_TYPE, ((PersonType)view.getTag()).getName());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);
            }
        });

        // 2. Capture controls of the view
        TextView name = (TextView) row.findViewById(R.id.person_type_name);
        ImageButton buttonNext = (ImageButton) row.findViewById(R.id.person_type_arrow_left);

        buttonNext.setTag(this.getItem(position));
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(),
                        PersonListActivity.class);
                i.putExtra(PersonTypeListActivity.EXTRA_PERSON_TYPE, ((PersonType) view.getTag()).getName());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);
            }
        });

        name.setText(getItem(position).getName());

        return row;
    }

}