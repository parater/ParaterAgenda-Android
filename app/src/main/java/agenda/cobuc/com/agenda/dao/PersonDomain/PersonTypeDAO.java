package agenda.cobuc.com.agenda.dao.persondomain;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import agenda.cobuc.com.agenda.lib.dao.QueryDAO;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;

public class PersonTypeDAO extends QueryDAO<PersonType> {

    public static final String PERSON_TYPE_TABLE_NAME = "PersonType";

    public static final String NAME_TYPE_ATTR = "type_name";


    public PersonTypeDAO(Context context) {
        super(context, PERSON_TYPE_TABLE_NAME);
        List<String> fields = new ArrayList<String>();
        fields.add(NAME_TYPE_ATTR);


        INSERT_QUERY = insertQueryBuilder(fields, PERSON_TYPE_TABLE_NAME);
    }

    @Override
    protected PersonType loadItemFromCursor(Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndex(NAME_TYPE_ATTR));

        return new PersonType(name);
    }

    @Override
    public void insert(List<PersonType> list) {
        HashSet<PersonType> set = new HashSet<>(list);
        list.clear();
        list.addAll(set);
        super.insert(list);
    }

    @Override
    protected SQLiteStatement loadBindings(SQLiteStatement statement, PersonType item) {
        statement.bindString(1, item.getName());
        return statement;
    }
}
