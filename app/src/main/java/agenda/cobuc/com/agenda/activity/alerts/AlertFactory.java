package agenda.cobuc.com.agenda.activity.alerts;


import android.app.AlertDialog;
import android.content.Context;

public class AlertFactory {

    public static AlertDialog.Builder cantConnectToWs(Context context) {
        return new AlertDialog.Builder(context).
                setTitle("Imposible actualizar").
                setMessage("Puede que el servidor no esté disponible en estos momentos o que usted no disponga de conexión a la red").
                setNeutralButton("De acuerdo", null);
    }

    public static AlertDialog.Builder malformedUrl(Context context) {
        return new AlertDialog.Builder(context).
                setTitle("Error en la URL").
                setMessage("La dirección web a la que apunta este objeto no es válida").
                setNeutralButton("De acuerdo", null);
    }

}
