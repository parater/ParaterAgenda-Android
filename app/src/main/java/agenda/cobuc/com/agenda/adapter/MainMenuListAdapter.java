package agenda.cobuc.com.agenda.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import agenda.cobuc.com.agenda.activity.PersonListActivity;
import agenda.cobuc.com.agenda.activity.PersonTypeListActivity;
import agenda.cobuc.com.agenda.models.Person;
import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.actions.Actions;
import agenda.cobuc.com.agenda.models.PersonType;

public class MainMenuListAdapter extends ArrayAdapter<MenuItem> {

    private List<MenuItem> data;

    private int layout;

    private Context context;

    public MainMenuListAdapter(Context context, int resource, List<MenuItem> data) {
        super(context, resource);
        this.context = context;
        this.data = data;
        this.layout = resource;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public MenuItem getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        // 1 .Create view
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(this.layout, parent, false);
            row.setTag(this.data.get(position));
        }

        row.setOnClickListener(this.getItem(position).getOnClickListener());

        // 2. Capture controls of the view
        TextView name = (TextView) row.findViewById(R.id.menu_item_name);
        ImageView image = (ImageView) row.findViewById(R.id.menu_item_image);
        ImageButton button = (ImageButton) row.findViewById(R.id.menu_item_arrow_left);

        name.setText(this.getItem(position).getName());
        image.setImageResource(this.getItem(position).getResource());
        button.setOnClickListener(this.getItem(position).getOnClickListener());
        button.setTag(this.getItem(position));

        return row;
    }

}