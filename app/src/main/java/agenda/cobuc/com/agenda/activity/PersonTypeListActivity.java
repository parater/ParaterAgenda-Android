package agenda.cobuc.com.agenda.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;

import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.activity.alerts.AlertFactory;
import agenda.cobuc.com.agenda.adapter.PersonTypeListAdapter;
import agenda.cobuc.com.agenda.dao.persondomain.PersonWithTypeDAO;
import agenda.cobuc.com.agenda.dao.persondomain.PersonTypeDAO;
import agenda.cobuc.com.agenda.models.PersonType;
import agenda.cobuc.com.agenda.models.PersonWithType;
import agenda.cobuc.com.agenda.models.PersonWithTypeSingleton;
import agenda.cobuc.com.agenda.util.Tuple2;

public class PersonTypeListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static String EXTRA_PERSON_TYPE = "person_type";

    private ListView listView;

    private SwipeRefreshLayout swipeRefresh;

    private List<PersonType> data;

    private PersonTypeListAdapter adapter;

    @Override
    protected void prepareView() {
        setContentView(R.layout.activity_person_type_list);

    }

    @Override
    protected void associateControls() {
        this.listView = (ListView) this.findViewById(R.id.person_type_listview);
        this.swipeRefresh = (SwipeRefreshLayout) this.findViewById(R.id.person_type_list_swipeRefreshLayout);
    }

    @Override
    protected void prepareActivity() {

        this.data = new LinkedList<>();
        this.configureSwipeRefresher();
        this.adapter = new PersonTypeListAdapter(PersonTypeListActivity.this, R.layout.person_type_list_item, this.data);
        this.listView.setAdapter(this.adapter);
        loadDataFromDatabase();
        //loadDataFromWebService();
    }

    private void loadDataFromDatabase() {
        final PersonTypeDAO personTypeDAO = new PersonTypeDAO(this.getApplicationContext());
        synchronized (data) {
            data.clear();
            data.addAll(personTypeDAO.getAll());
        }
        adapter.notifyDataSetChanged();
    }

    private void loadDataFromWebService() {

        final PersonWithTypeDAO personWithTypeDAO = new PersonWithTypeDAO(this.getApplicationContext());
        final PersonTypeDAO personTypeDAO = new PersonTypeDAO(this.getApplicationContext());
                // try to make a WebService call
        new agenda.cobuc.com.agenda.WebServices.Person().getPersonList(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray json) {
                // if the ws returns something, delete all data from the database and update it
                try {
                    synchronized (data) {
                        List<PersonWithType> jsonResponse = PersonWithTypeSingleton.getInstance().fromJson(json);
                        Tuple2<List<PersonWithType>, List<PersonType>> tuple = PersonWithTypeSingleton.split(jsonResponse);

                        data.clear();

                        personTypeDAO.deleteAll();
                        personWithTypeDAO.deleteAll();

                        personTypeDAO.insert(tuple._2);
                        personWithTypeDAO.insert(tuple._1);

                        data.addAll(personTypeDAO.getAll());

                        swipeRefresh.setRefreshing(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    synchronized (data) {
                        loadDataFromWebServiceErrorRoutine();
                    }
                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                synchronized (data) {
                    loadDataFromWebServiceErrorRoutine();
                }
            }
        }, getApplicationContext());
    }

    private void loadDataFromWebServiceErrorRoutine() {
        final PersonTypeDAO personTypeDAO = new PersonTypeDAO(this.getApplicationContext());
        data.clear();
        data.addAll(personTypeDAO.getAll());
        adapter.notifyDataSetChanged();
        swipeRefresh.setRefreshing(false);
        AlertFactory.cantConnectToWs(PersonTypeListActivity.this).show();
    }

    private void configureSwipeRefresher() {
        this.swipeRefresh.setOnRefreshListener(this);
        this.swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_dark,
                android.R.color.holo_red_light);
    }

    @Override
    public void onRefresh() {

        loadDataFromWebService();
    }
}
