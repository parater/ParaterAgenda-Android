package agenda.cobuc.com.agenda.WebServices;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

public class Person {

    private String domain = "http://192.168.1.36:9000";
    private String personListUri = "/person/";

    public void getPersonList(
            Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener,
            Context context) {

        String personListUrl = domain + personListUri;

        JsonArrayRequest personListRequest =
                new JsonArrayRequest(Request.Method.GET, personListUrl,
                        responseListener, errorListener);

        VolleySingleton.getInstance(context).addToRequestQueue(personListRequest);

    }
}
