package agenda.cobuc.com.agenda.adapter;

import android.view.View;

public class MenuItem {

    private int resource;

    private String name;

    private View.OnClickListener onClickListener;

    public MenuItem(String name, int resource, View.OnClickListener onClickListener) {
        this.resource = resource;
        this.name = name;
        this.onClickListener = onClickListener;
    }

    public int getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }
}
