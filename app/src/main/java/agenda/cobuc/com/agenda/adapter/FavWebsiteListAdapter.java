package agenda.cobuc.com.agenda.adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import agenda.cobuc.com.agenda.R;
import agenda.cobuc.com.agenda.actions.Actions;
import agenda.cobuc.com.agenda.activity.alerts.AlertFactory;
import agenda.cobuc.com.agenda.models.FavWebsite;

public class FavWebsiteListAdapter extends ArrayAdapter<FavWebsite>{

    private List<FavWebsite> data;

    private int layout;

    private Context context;

    public FavWebsiteListAdapter(Context context, int resource, List<FavWebsite> data) {
        super(context, resource);
        this.context = context;
        this.data = data;
        this.layout = resource;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public FavWebsite getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        // 1 .Create view
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(this.layout, parent, false);
            row.setTag(this.data.get(position));
        }

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!Patterns.WEB_URL.matcher(getItem(position).getUrl()).matches()) {
                    AlertFactory.malformedUrl(context).show();
                } else {
                    Actions.getInstance().openInBrowser(context, Uri.parse(getItem(position).getUrl()));
                }
            }
        });

        // 2. Capture controls of the view
        TextView name = (TextView) row.findViewById(R.id.favwebsite_item_name);

        name.setText(this.getItem(position).getLabel());

        return row;
    }
}
