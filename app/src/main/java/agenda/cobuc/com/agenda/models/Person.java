package agenda.cobuc.com.agenda.models;

import org.json.JSONStringer;

/**
 * A person of the domain model
 */
public class Person {

    private String name;

    private String surname1;

    private String surname2;

    private String phone;

    private String email;

    public Person(String name, String surname1, String surname2, String phone, String email) {
        this.name = name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getSurname1() {
        return surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

}
